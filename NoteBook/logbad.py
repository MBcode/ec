#!/usr/bin/env python3
#broken out from mknb.py
from flask import Flask
app = Flask(__name__)
from flask import request

#can bake in server HOST & MKNB_PORT into the NBs, or pass in, or just email; so maybe don't use this
@app.route('/logbad/')  #have try/except, so log errors soon, also have 'log' file in NB from wget/etc
def log_bad():
    dwnurl_str = request.args.get('url',  type = str)
    lbs= f'log_bad:url={dwnurl_str}'
    print(lbs) #just in the log for now
    err = request.args.get('error', default = 'None',   type = str)
    if err:
        print(f'error:{err}')
    else:
        err=""
    return r+err

@app.route('/alive/')
def alive():
    return "alive"

if __name__ == '__main__':
    import sys
    if(len(sys.argv)>1):
        dwnurl_str = sys.argv[1]
        if(len(sys.argv)>2):
            ext=sys.argv[2]
        else:
            ext=None
        g = gist_api.get_gists() #set the global w/fresh value
        r=mknb(dwnurl_str, ext) #or trf.py test, that will be in ipynb template soon
        print(r)
    else: #w/o args, just to run a service:
        app.run(host='0.0.0.0', port=8004, debug=True)
        #os.environ["MKNB_PORT"]=port #won't help in nb
