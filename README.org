** most changes are made on github
** metadata-search of Dataset/resources
*** started with:
crawl of embedded metadata for each of their landing pages, and search in a triplestore
*** this repo started to:
share NotebBooks..(showing this off), had more dev in colab because of secret keys/etc, but open now
*** Also 
metadata crawling and asserting to clowderframework for search
*** Use: 
use of the search-API adaptor is being expanded in ./search
*** Qry:
SPARQL/etc as we transition back to triplestore and what we can do w/it
*** Prep
Parts will soon be put in: https://github.com/earthcube/earthcube-utilities
*** [[http://mbobak-ofc.ncsa.illinois.edu/about.htm][diagram]]:
w/[[links.txt][links]] embedded in it
[[ec.svg]]
*** Latest work going right into alpha production [[https://alpha.geocodes.earthcube.org/][site]]
https://github.com/earthcube/geodexui now also here in geodexUI, test ./NoteBook launching
*** Recent [[http://isda.ncsa.uiuc.edu/~mbobak/sd/][talk]] about the work
